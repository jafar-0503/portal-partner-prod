#!/bin/bash

if [ "$BUILD_WAR" == 1 ]; then
  sed -i -e "s/\(<packaging>\).*\(<\/packaging>\)/\1war\2/" $1
  sed -i -e "s/\(<scope>\)compile\(<\/scope>\)/\1provided\2/" $1
fi
