package id.equity.portalpartner.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by jupiterzhuo on 20/05/19.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "m_app")
public class Application  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "application")
    private Set<Module> modules;

    public Application(String name) {
        this.name = name;
    }
}