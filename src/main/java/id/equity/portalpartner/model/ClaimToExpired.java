package id.equity.portalpartner.model;

import id.equity.portalpartner.model.enums.ClaimToExpiredStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@Table(name = "t_claim_to_expired")
public class ClaimToExpired {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "claim_reg_no", length = 100)
    private String claimRegNo;

    @Column(name = "status")
    private ClaimToExpiredStatus status;

    @Column(name = "expired_at")
    private Date expiredAt;

    public ClaimToExpired(Long id, String claimRegNo, ClaimToExpiredStatus status, Date expiredAt) {
        this.id = id;
        this.claimRegNo = claimRegNo;
        this.status = status;
        this.expiredAt = expiredAt;
    }
}
