package id.equity.portalpartner.model;

import id.equity.portalpartner.config.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@Table(name = "m_company_policy")
public class CompanyPolicy extends Auditable<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "policy_no", length = 20)
    private String policyNo;

    @Column(length = 20)
    private String name;

    @Column(name = "edits_form_setting")
    private Integer editsFormSetting;

    private boolean status;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "source_id")
    private SourcePolicy sourcePolicy;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "flow_id")
    private ClaimFlow claimFlow;

    public CompanyPolicy(String policyNo, String name, Integer editsFormSetting, boolean status, boolean isDeleted, Company company, SourcePolicy sourcePolicy, ClaimFlow claimFlow) {
        this.policyNo = policyNo;
        this.name = name;
        this.editsFormSetting = editsFormSetting;
        this.status = status;
        this.isDeleted = isDeleted;
        this.company = company;
        this.sourcePolicy = sourcePolicy;
        this.claimFlow = claimFlow;
    }
}
