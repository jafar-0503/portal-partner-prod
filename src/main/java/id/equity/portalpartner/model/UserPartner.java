package id.equity.portalpartner.model;

import id.equity.portalpartner.config.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@Table(name = "m_user_partner")
public class UserPartner extends Auditable<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100)
    private String name;

    @Column(length = 100, unique = true)
    @NotNull(message = "Username cannot be null")
    private String username;

    @Column(name = "password",length = 100)
    private String password;

    @Column(length = 100)
    private String email;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "group_id")
    private Group group;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id")
    private Company company;

    public UserPartner(String name, String username, String email, Group group, Company company) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.group = group;
        this.company = company;
    }
}
