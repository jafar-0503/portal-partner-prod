package id.equity.portalpartner.enums;

public enum StatusEnum {
    SUBMIT,
    CANCEL,
    SAVE
}
