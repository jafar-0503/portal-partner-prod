package id.equity.portalpartner.config.security;

public class SecurityConstants {
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String SECRET = "YdMTWgyxEmeoQEAy0hSBqeLqHQMBVB3McwHejPOFJ8BRDFJXqpKXigZbYAuOoz1";
    public static final String SIGN_UP_URL = "/api/v1/users-partners";
    public static final String TOKEN_PREFIX = "Bearer ";
}
