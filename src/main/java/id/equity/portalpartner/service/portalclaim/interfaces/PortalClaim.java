package id.equity.portalpartner.service.portalclaim.interfaces;

import id.equity.portalpartner.dto.portalclaim.*;
import id.equity.portalpartner.service.portalclaim.PortalClaimBaseRequest;
import id.equity.portalpartner.service.portalclaim.PortalClaimBaseResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import java.util.List;

public interface PortalClaim {
    @POST(".")
    Call<PortalClaimBaseResponse<List<InsuredDto>>> getListInsured(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<IllnessDto>>> getListIllness(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<ClaimTypeDto>>> getListClaimType(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<BankDto>>> getListBank(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<RelationInsuredDto>>> getListRelationInsured(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<CurrencyDto>>> getListCurrency(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<ClaimStatusDto>>> getListClaimStatus(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<ClaimPartnerDto>>> getListClaimPartner(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<ClaimHistoryProcessDto>>> getListClaimHistoryProcess(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<ClaimDocumentFileDto>>> getListClaimDocumentFile(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<ClaimDocumentDto>>> getListClaimDocument(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<EformDto>>> getListEform(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<EformDocumentDto>>> getListEformDocument(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<ClaimTransactionResponseDto>> createClaimTransaction(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<DocumentDto>>> getListDocument(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<DocumentFileDto>> getDocument(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<UpdatedDocumentFileDto>> updateDocumentFile(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<UpdatedClaimDto>>> submitClaim(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<UpdatedClaimDto>>> saveClaim(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<UpdatedClaimDto>>> cancelClaim(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<AddedDocumentFileDto>> addDocumentFile(@Body PortalClaimBaseRequest request);

    @POST(".")
    Call<PortalClaimBaseResponse<List<UpdatedClaimDto>>> expiredClaim(@Body PortalClaimBaseRequest request);
}
