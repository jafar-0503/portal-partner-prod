package id.equity.portalpartner.service;

import id.equity.portalpartner.model.Company;
import id.equity.portalpartner.model.CompanyPolicy;
import id.equity.portalpartner.repository.CompanyPolicyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyPolicyService {
    @Autowired
    private CompanyPolicyRepository companyPolicyRepository;

    public Iterable<CompanyPolicy> findCompanyPolicyByNameAndCompany(String name, Company company, Boolean isDeleted) {
        Iterable<CompanyPolicy> companyPolicies =
                companyPolicyRepository.findCompanyPolicyByNameContainingAndCompanyAndIsDeleted(name, company, isDeleted);

        return companyPolicies;
    }
}
