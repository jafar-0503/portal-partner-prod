package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.portalclaim.*;
import id.equity.portalpartner.service.portalclaim.PortalClaimService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("api/v1/ex/")
public class PortalClaimController {
    @Autowired
    private PortalClaimService portalClaimService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    @RequestMapping("insured")
    public ResponseEntity<BaseResponse<List<InsuredDto>>> getInsured(
            @RequestParam(name = "policy_no") String policyNo,
            @RequestParam(name = "source_code") String sourceId,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<InsuredDto> insuredDtos = portalClaimService.getListInsured(params);
        Type targetListType = new TypeToken<List<InsuredDto>>() {}.getType();
        List<InsuredDto> mapped = modelMapper.map(insuredDtos, targetListType);
        BaseResponse<List<InsuredDto>> response =
            new BaseResponse<>(true, mapped, "Insured retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("illness")
    public ResponseEntity<BaseResponse<List<IllnessDto>>> getIllness(
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<IllnessDto> illnessDtos = portalClaimService.getListIllness(params);
        Type targetListType = new TypeToken<List<IllnessDto>>() {}.getType();
        List<IllnessDto> mapped = modelMapper.map(illnessDtos, targetListType);
        BaseResponse<List<IllnessDto>> response =
                new BaseResponse(true, mapped, "Illness retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("claim-types")
    public ResponseEntity<BaseResponse<List<ClaimTypeDto>>> getClaimType(
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam(name = "policy_id", required = false) Long policyId,
            @RequestParam(name = "is_used_by_customer", defaultValue = "true") boolean isUsedByCustomer,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<ClaimTypeDto> claimTypeDtos = portalClaimService.getListClaimType(policyId, isUsedByCustomer, params);
        Type targetListType = new TypeToken<List<ClaimTypeDto>>() {}.getType();
        List<ClaimTypeDto> mapped = modelMapper.map(claimTypeDtos, targetListType);
        BaseResponse<List<ClaimTypeDto>> response =
                new BaseResponse(true, mapped, "Claim type retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("banks")
    public ResponseEntity<BaseResponse<List<BankDto>>> getBank(
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<BankDto> bankDtos = portalClaimService.getListBank(params);
        Type targetListType = new TypeToken<List<BankDto>>() {}.getType();
        List<BankDto> mapped = modelMapper.map(bankDtos, targetListType);
        BaseResponse<List<BankDto>> response =
                new BaseResponse(true, mapped, "Bank retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("relation-insured")
    public ResponseEntity<BaseResponse<List<RelationInsuredDto>>> getRelationInsured(
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<RelationInsuredDto> relationInsuredDtos = portalClaimService.getListRelationInsured(params);
        Type targetListType = new TypeToken<List<RelationInsuredDto>>() {}.getType();
        List<RelationInsuredDto> mapped = modelMapper.map(relationInsuredDtos, targetListType);
        BaseResponse<List<RelationInsuredDto>> response =
                new BaseResponse(true, mapped, "Relation insured retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("currency")
    public ResponseEntity<BaseResponse<List<CurrencyDto>>> getCurrency(
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<CurrencyDto> currencyDtos = portalClaimService.getListCurrency(params);
        Type targetListType = new TypeToken<List<CurrencyDto>>() {}.getType();
        List<CurrencyDto> mapped = modelMapper.map(currencyDtos, targetListType);
        BaseResponse<List<CurrencyDto>> response =
                new BaseResponse(true, mapped, "Currency retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("claim-status")
    public ResponseEntity<BaseResponse<List<ClaimStatusDto>>> getClaimStatus(
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<ClaimStatusDto> claimStatusDtos = portalClaimService.getListClaimStatus(params);
        Type targetListType = new TypeToken<List<ClaimStatusDto>>() {}.getType();
        List<ClaimStatusDto> mapped = modelMapper.map(claimStatusDtos, targetListType);
        BaseResponse<List<ClaimStatusDto>> response =
                new BaseResponse(true, mapped, "Claim status retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("claim-partners")
    public ResponseEntity<BaseResponse<List<ClaimPartnerDto>>> getClaimPartner(
            @RequestParam(name = "partner") String partner,
            @RequestParam(name = "claim_status_code", required = false) String claimStatusCode,
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<ClaimPartnerDto> claimPartnerDtos = portalClaimService.getListClaimPartner(params);
        BaseResponse<List<ClaimPartnerDto>> response =
                new BaseResponse(true, claimPartnerDtos, "Claim partner retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("claim-history-process")
    public ResponseEntity<BaseResponse<List<ClaimHistoryProcessDto>>> getClaimHistoryProcess(
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam(name = "claim_reg_no") String claimRegNo,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<ClaimHistoryProcessDto> claimHistoryProcessDtos = portalClaimService.getListClaimHistoryProcess(params);
        BaseResponse<List<ClaimHistoryProcessDto>> response =
                new BaseResponse(true, claimHistoryProcessDtos, "Claim history process retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("claim-document-files")
    public ResponseEntity<BaseResponse<List<ClaimDocumentFileDto>>> getClaimDocumentFile(
            @RequestParam(name = "transaction_document_id", required = false) String transactionDocumentId,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<ClaimDocumentFileDto> claimDocumentFileDtos = portalClaimService.getListClaimDocumentFile(params);
        Type targetListType = new TypeToken<List<ClaimDocumentFileDto>>() {}.getType();
        List<ClaimHistoryProcessDto> mapped = modelMapper.map(claimDocumentFileDtos, targetListType);
        BaseResponse<List<ClaimDocumentFileDto>> response =
                new BaseResponse(true, mapped, "Claim document file retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("claim-documents")
    public ResponseEntity<BaseResponse<List<ClaimDocumentDto>>> getClaimDocument(
            @RequestParam(name = "transaction_id") String transactionId,
            @RequestParam Map<String, String> params
    ) throws IOException, ResourceNotFoundException {
        List<ClaimDocumentDto> claimDocumentDtos = portalClaimService.getListClaimDocument(params);
        BaseResponse<List<ClaimDocumentDto>> response =
                new BaseResponse(true, claimDocumentDtos, "Claim document retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("eforms")
    public ResponseEntity<BaseResponse<List<EformDto>>> getEform(
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<EformDto> eformDtos = portalClaimService.getListEform(params);
        BaseResponse<List<EformDto>> response =
                new BaseResponse(true, eformDtos, "Eform retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("eform-documents")
    public ResponseEntity<BaseResponse<List<EformDocumentDto>>> getEformDocument(
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam(name = "eform_id", required = false) Long eformId,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<EformDocumentDto> eformDocumentDtos = portalClaimService.getListEformDocument(params);
        BaseResponse<List<EformDocumentDto>> response =
                new BaseResponse(true, eformDocumentDtos, "Eform documents retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @GetMapping
    @RequestMapping("documents")
    public ResponseEntity<BaseResponse<List<DocumentDto>>> getDocument(
            @RequestParam(name = "is_delete", defaultValue = "0") String isDelete,
            @RequestParam(name = "claim_id", required = false) Long claimType,
            @RequestParam Map<String, String> params
    ) throws IOException {
        List<DocumentDto> documentDtos = portalClaimService.getListDocument(claimType, params);
        BaseResponse<List<DocumentDto>> response =
                new BaseResponse(true, documentDtos, "Documents retrieved successfully");

        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "documents-files", method = RequestMethod.GET)
    public ResponseEntity<BaseResponse<DocumentFileDto>> getDocumentFile(
            @RequestParam(name = "claim_reg_no") String claimRegNo,
            @RequestParam(name = "document_url") String documentUrl,
            @RequestParam Map<String, String> params
    ) throws IOException {
        DocumentFileDto documentFileDto = portalClaimService.getDocumentFile(params);
        BaseResponse<DocumentFileDto> response;

        if (documentFileDto.getStatus().equals("success")) {
            response = new BaseResponse(true, documentFileDto, "Documents file retrieved successfully");
        } else {
            response = new BaseResponse<>(false, documentFileDto, "Documents file failed to retrieve");
        }

        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "documents-files", method = RequestMethod.POST)
    public ResponseEntity<BaseResponse<AddedDocumentFileDto>> createDocumentFile(
            @RequestBody AddDocumentFileDto addDocumentFileDto
    ) throws IOException {
        AddedDocumentFileDto addedDocumentFileDto = portalClaimService.addDocumentFile(addDocumentFileDto);
        BaseResponse<AddedDocumentFileDto> response;

        if (addedDocumentFileDto == null) {
            response = new BaseResponse(true, addedDocumentFileDto, "Documents file added successfully");
        } else {
            response = new BaseResponse<>(false, addedDocumentFileDto, "Documents file failed to add");
        }

        return ResponseEntity.ok(response);
    }

    @PatchMapping
    @RequestMapping("documents-files/{transactionDocumentFileId}")
    public ResponseEntity<BaseResponse<UpdatedDocumentFileDto>> updateDocumentFile(
            @PathVariable(name = "transactionDocumentFileId") String transactionDocumentFileId,
            @RequestBody Map<String, String> documentFile
    ) throws IOException {
        UpdatedDocumentFileDto updatedDocumentFileDto = portalClaimService.updateDocumentFile(
                transactionDocumentFileId,
                documentFile
        );
        BaseResponse<UpdatedDocumentFileDto> response;

        if (updatedDocumentFileDto.getAffected() != 1) {
            response =
                    new BaseResponse(false, updatedDocumentFileDto, "Documents file failed to update");
        } else {
            response =
                    new BaseResponse(true, updatedDocumentFileDto, "Documents file updated successfully");
        }

        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "claims", method = RequestMethod.POST)
    public ResponseEntity<BaseResponse<ClaimTransactionResponseDto>> createClaim(
            @RequestBody ClaimTransactionDto claimTransactionDto
    ) throws IOException, ResourceNotFoundException {
        ClaimTransactionResponseDto res = portalClaimService.createClaimTransaction(claimTransactionDto);
        BaseResponse<ClaimTransactionResponseDto> response;

        if (res.getResult().toString().equals("failed")) {
            response = new BaseResponse(false, res, "Create claim transaction failed");
        } else {
            response = new BaseResponse(true, res, "Create claim transaction successfully");
        }

        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "claims/submit", method = RequestMethod.PATCH)
    public ResponseEntity<BaseResponse<UpdatedClaimDto>> submitClaim(
            @RequestBody @Valid SubmitCancelClaimDto body
    ) throws IOException, IllegalAccessException, ResourceNotFoundException {
        UpdatedClaimDto res = portalClaimService.submitClaim(body);

        BaseResponse<UpdatedClaimDto> response;

        if (res.getStatus().toString().equals("1")) {
            response = new BaseResponse(true, res, "Claim submitted successfully");
        } else {
            response = new BaseResponse(false, res, "Submit claim failed");
        }

        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "claims/save", method = RequestMethod.PATCH)
    public ResponseEntity<BaseResponse<UpdatedClaimDto>> saveClaim(
            @RequestBody @Valid SaveClaimDto body
    ) throws IOException, IllegalAccessException {
        UpdatedClaimDto res = portalClaimService.saveClaim(body);

        BaseResponse<UpdatedClaimDto> response;

        if (res.getStatus().toString().equals("1")) {
            response = new BaseResponse(true, res, "Claim saved successfully");
        } else {
            response = new BaseResponse(false, res, "Save claim failed");
        }

        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "claims/cancel", method = RequestMethod.PATCH)
    public ResponseEntity<BaseResponse<UpdatedClaimDto>> cancelClaim(
            @RequestBody @Valid CancelClaimDto body
    ) throws IOException, IllegalAccessException {
        UpdatedClaimDto res = portalClaimService.cancelClaim(body);

        BaseResponse<UpdatedClaimDto> response;

        if (res.getStatus().toString().equals("1")) {
            response = new BaseResponse(true, res, "Claim cancelled successfully");
        } else {
            response = new BaseResponse(false, res, "Cancel claim failed");
        }

        return ResponseEntity.ok(response);
    }
}
