package id.equity.portalpartner.controller;

import id.equity.portalpartner.dto.dashboard.ClaimStatusCountDto;
import id.equity.portalpartner.service.DashboardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/dashboards")
public class DashboardController {
    @Autowired
    DashboardService dashboardService;

    @GetMapping
    public List<ClaimStatusCountDto> find(
            @RequestParam(name = "claim_status_code", required = false) String claimStatuseCode,
            @RequestParam(name = "partner", required = false) String partner
    ) {
        List<ClaimStatusCountDto> claimStatusCountDtoList = dashboardService.claimStatusCountList(claimStatuseCode, partner);

        return claimStatusCountDtoList;
    }
}
