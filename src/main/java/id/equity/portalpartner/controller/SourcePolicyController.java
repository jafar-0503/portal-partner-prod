package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.sourcepolicy.SourcePolicyDto;
import id.equity.portalpartner.model.SourcePolicy;
import id.equity.portalpartner.repository.SourcePolicyRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/source-policies")
public class SourcePolicyController {
    @Autowired
    private SourcePolicyRepository sourcePolicyRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<SourcePolicyDto>>> find() {
        Iterable<SourcePolicy> companies = sourcePolicyRepository.findAll();
        Type targetListType = new TypeToken<List<SourcePolicyDto>>() {}.getType();
        List<SourcePolicyDto> sourcePolicy = modelMapper.map(companies, targetListType);
        BaseResponse<List<SourcePolicyDto>> listBaseResponse =
                new BaseResponse<>(true, sourcePolicy, "Source policy retrieved successfully");

        return ResponseEntity.ok(listBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<SourcePolicy>> create(@Valid @RequestBody SourcePolicyDto sourcePolicyDto) {
        SourcePolicy sourcePolicy = modelMapper.map(sourcePolicyDto, SourcePolicy.class);
        SourcePolicy sourcePolicy1 = sourcePolicyRepository.save(sourcePolicy);
        BaseResponse<SourcePolicy> sourcePolicyBaseResponse =
                new BaseResponse<>(true, sourcePolicy1, "Source policy created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(sourcePolicyBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<SourcePolicyDto>> get(@PathVariable Long id) throws ResourceNotFoundException {
        SourcePolicy sourcePolicy = sourcePolicyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        SourcePolicyDto sourcePolicy1 = modelMapper.map(sourcePolicy, SourcePolicyDto.class);
        BaseResponse<SourcePolicyDto> sourcePolicyBaseResponse =
                new BaseResponse<>(true, sourcePolicy1, "Source policy retrieved successfully");

        return ResponseEntity.ok(sourcePolicyBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<SourcePolicy>> patch(@PathVariable Long id, @Valid @RequestBody SourcePolicyDto sourcePolicyDto) throws ResourceNotFoundException {
        sourcePolicyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        SourcePolicy sourcePolicy = modelMapper.map(sourcePolicyDto, SourcePolicy.class);
        sourcePolicy.setId(id);

        SourcePolicy sourcePolicy1 = sourcePolicyRepository.save(sourcePolicy);
        BaseResponse<SourcePolicy> sourcePolicyBaseResponse =
                new BaseResponse<>(true, sourcePolicy1, "Source policy patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(sourcePolicyBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<SourcePolicy>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        SourcePolicy sourcePolicy = sourcePolicyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        SourcePolicy sourcePolicy1 = modelMapper.map(sourcePolicy, SourcePolicy.class);

        sourcePolicyRepository.delete(sourcePolicy1);

        BaseResponse<SourcePolicy> sourcePolicyBaseResponse =
                new BaseResponse<>(true, sourcePolicy1, "Source policy deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(sourcePolicyBaseResponse);
    }
}
