package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.Group;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GroupRepository extends PagingAndSortingRepository<Group, Long> {
}
