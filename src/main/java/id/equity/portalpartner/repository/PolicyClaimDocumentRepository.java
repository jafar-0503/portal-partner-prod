package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.PolicyClaimDocument;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PolicyClaimDocumentRepository extends PagingAndSortingRepository<PolicyClaimDocument, Integer> {
    List<PolicyClaimDocument> findByCompanyClaimId(Long claimId);
    List<PolicyClaimDocument> findByCompanyClaimClaimTypeOrderByDocumentTypeAsc(String claimType);
    PolicyClaimDocument findByCompanyClaimIdAndDocumentType(Long claimId, String documentCode);
}
