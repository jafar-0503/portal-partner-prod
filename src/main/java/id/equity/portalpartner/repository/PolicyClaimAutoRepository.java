package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.PolicyClaimAuto;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PolicyClaimAutoRepository extends PagingAndSortingRepository<PolicyClaimAuto, Integer> {
    Iterable<PolicyClaimAuto> findByCompanyClaimIdAndClaimTypeNotInOrderByClaimTypeAsc(Long claimId, String claimType);
}
