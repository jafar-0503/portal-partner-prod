package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.Module;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ModuleRepository extends PagingAndSortingRepository<Module, Long> {
    Iterable<Module> findByGroupsId(Long groupId);
}
