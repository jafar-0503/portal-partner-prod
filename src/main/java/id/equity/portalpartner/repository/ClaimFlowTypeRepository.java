package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.ClaimFlowType;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClaimFlowTypeRepository extends PagingAndSortingRepository<ClaimFlowType, Long> {
    Iterable<ClaimFlowType> findByClaimFlowId(Long claimFlowId);
}
