package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.CompanyClaim;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface CompanyClaimRepository extends PagingAndSortingRepository<CompanyClaim, Long> {
    Iterable<CompanyClaim> findByCompanyPolicyIdAndIsUsedByCustomer(Long policyId, boolean isUsedByCustomer);
    Iterable<CompanyClaim> findByCompanyPolicyIdAndClaimType(Long policyId, String claimType);
    Iterable<CompanyClaim> findByCompanyPolicyId(Long policyId);
    Optional<CompanyClaim> findByCompanyPolicyPolicyNoAndClaimType(String policyNo, String claimType);
}
