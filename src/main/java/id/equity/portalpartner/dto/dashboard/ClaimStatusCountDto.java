package id.equity.portalpartner.dto.dashboard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimStatusCountDto {
    private String claimStatusCode;
    private Integer count;
}
