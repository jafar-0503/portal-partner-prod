package id.equity.portalpartner.dto.policyclaimdocument;

import id.equity.portalpartner.model.PolicyClaimDocument;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreatePolicyClaimDocumentDto extends PolicyClaimDocument {
    private String claimId;
}
