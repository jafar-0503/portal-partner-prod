package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IllnessDto {
    @SerializedName("illness_id")
    private Long illnessId;
    @SerializedName("illness_code")
    private String illnessCode;
    private String name;
    @SerializedName("status_description")
    private String statusDescription;
    @SerializedName("is_delete")
    private Integer isDelete;
}
