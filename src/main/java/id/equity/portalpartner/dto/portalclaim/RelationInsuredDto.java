package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RelationInsuredDto {
    @SerializedName("relation_insured_id")
    private Long relationInsuredId;
    @SerializedName("relation_insured_code")
    private String relationInsuredCode;
    @SerializedName("relation_insured_name")
    private String relationInsuredName;
    @SerializedName("is_delete")
    private Integer isDelete;
}
