package id.equity.portalpartner.dto.portalclaim;

import id.equity.portalpartner.enums.StatusEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UpdateClaimDto {
    private StatusEnum status;
}
