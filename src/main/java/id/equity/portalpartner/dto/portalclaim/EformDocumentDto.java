package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EformDocumentDto {
    @SerializedName("eform_document_id")
    private Long eformDocumentId;
    @SerializedName("eform_id")
    private Long eformId;
    @SerializedName("document_id")
    private Long documentId;
    @SerializedName("document_code")
    private String documentCode;
    @SerializedName("description")
    private String description;
    @SerializedName("minimum_files")
    private String minimum_files;
    @SerializedName("maximum_files")
    private String maximum_files;
    @SerializedName("applies_to")
    private String applies_to;
    @SerializedName("seq_number")
    private String seq_number;
    @SerializedName("status_id")
    private Long statusId;
    @SerializedName("dms_code")
    private String dmsCode;
}
