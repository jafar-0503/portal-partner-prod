package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class InsuredDto {
    @SerializedName("insured_no")
    private String insuredNo;
    @SerializedName("insured_name")
    private String insuredName;
    @SerializedName("ktp_no")
    private String ktpNo;
    @SerializedName("date_of_birth")
    private String dateOfBirth;
    @SerializedName("id_member_partner")
    private String idMemberPartner;
    @SerializedName("policy_no")
    private String policyNo;
    @SerializedName("product_name")
    private String productName;
    @SerializedName("insurance_period_from")
    private String insurancePeriodFrom;
    @SerializedName("insurance_period_to")
    private String insurancePeriodTo;
    private String currency;
    @SerializedName("sum_assured")
    private Long sumAssured;
    @SerializedName("phone_no")
    private String phoneNo;
    @SerializedName("email")
    private String email;
    @SerializedName("virtual_account")
    private String virtualAccount;
}
