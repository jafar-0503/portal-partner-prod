package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CancelClaimDto {
    @SerializedName("transaction_id")
    private String transactionId;
    @SerializedName("transaction_form_id")
    private String transactionFormId;
    private String remarks;
    @SerializedName("id_user_partner")
    private String idUserPartner;
}
