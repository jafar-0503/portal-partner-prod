package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class AddDocumentFileDto {
    @SerializedName("claim_reg_no")
    private String claimRegNo;
    List<ClaimTransactionDocumentsDto> documents;
}
