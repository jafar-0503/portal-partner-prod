package id.equity.portalpartner.dto.claimflowtype;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.equity.portalpartner.dto.claimflow.ClaimFlowDto;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ListClaimFlowTypeDto extends ClaimFlowTypeDto {
    @JsonManagedReference
    private ClaimFlowDto claimFlow;
}
