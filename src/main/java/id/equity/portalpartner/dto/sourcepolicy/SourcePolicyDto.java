package id.equity.portalpartner.dto.sourcepolicy;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SourcePolicyDto {
    private Long id;
    private String name;
    private String code;
}
