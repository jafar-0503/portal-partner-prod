TARGET_DIR=build
CONTAINER_ID=0
PROJECT_NAME=$(notdir $(PWD))
LOWER_PROJECT_NAME=$(shell echo $(PROJECT_NAME) | tr A-Z a-z)

all: help

help:
	@echo ''
	@echo 'Usage: make [TARGET]'
	@echo 'Targets:'
	@echo '	 build-jar		docker build . -t $(LOWER_PROJECT_NAME) --target MAVEN_TOOL_CHAIN'
	@echo '	 build-war		docker build . -t $(LOWER_PROJECT_NAME) --target MAVEN_TOOL_CHAIN --build-arg BUILD_WAR=1'
	@echo '	 clean			rm -rf $(TARGET_DIR)'
	@echo ''

complete-msg:
	@echo "Compiled files is in \"$(CURDIR)/$(TARGET_DIR)\" directory ✨"

build-jar:
	docker build . -t $(LOWER_PROJECT_NAME) --target MAVEN_TOOL_CHAIN
	@make build
	@make complete-msg

build-war:
	docker build . -t $(LOWER_PROJECT_NAME) --target MAVEN_TOOL_CHAIN --build-arg BUILD_WAR=1
	@make build
	@make complete-msg

build:
	$(eval CONTAINER_ID=$(shell docker create $(LOWER_PROJECT_NAME)))
	docker cp $(CONTAINER_ID):/tmp/target $(TARGET_DIR)
	docker rm $(CONTAINER_ID)

clean:
	rm -rf $(TARGET_DIR)
